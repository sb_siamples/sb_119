package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb119Application {

	public static void main(String[] args) {
		SpringApplication.run(Sb119Application.class, args);
	}

}

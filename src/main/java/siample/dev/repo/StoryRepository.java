package siample.dev.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import siample.dev.domain.Story;

public interface StoryRepository extends CrudRepository<Story, Long> {

	List<Story> findAll(); // Iterable felülirasa CrudRepo-bol

	Story findFirstByOrderByPostedDesc(); // JPA sql nyelvezete

	/*
	 * SELECT * FROM STORY WHERE posted IN (SELECT max(posted) FROM story) LIMIT 1;
	 */

	Story findByTitle(String title); // JPA sql nyelvezete

}
